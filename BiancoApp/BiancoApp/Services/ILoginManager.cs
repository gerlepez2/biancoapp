﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BiancoApp.Services
{
    public interface ILoginManager
    {
        void ShowMainPage();
        void Logout();
    }
}
