﻿using BiancoApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace BiancoApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginView : ContentPage
	{
        public static string usuario = "";
        public static string password = "";

        ILoginManager iml = null;

        public LoginView (ILoginManager ilm)
		{
            InitializeComponent ();
            iml = ilm;
		}

        async void btnLoginClick(object sender, EventArgs e)
        {

            usuario = Username.Text;
            password = Password.Text;

            //Validación de usuario con base de datos

            if (usuario == "David" && password == "123")
            {
                App.Current.Properties["Name"] = usuario;
                App.Current.Properties["IsLoggedIn"] = true;
                iml.ShowMainPage();
            }
            else
            {
                await DisplayAlert("", "Credenciales Incorrectas", "Ok");
            }



        }

    }
}