﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BiancoApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HomeView : ContentPage
    {
        private string nombreUsuario;
        public static string NombreUsuario;
        ContentPage home;

        public HomeView()
        {

            InitializeComponent();
        }

        void btnLogoutClick(object sender, EventArgs e)
        {
            App.Current.Logout();
        }

        void ClickFacturacion(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new FacturacionView());
        }
        void ClickCobranza(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new CobranzaView());
        }
        void ClickReclamos(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new ReclamosView());
        }
        void ClickInformeVisita(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new InformeVisitaView());
        }



    }
}
